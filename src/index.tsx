import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { App } from './app/app';
/***
 * Bootstrap the app
 */
ReactDOM.render(<Router><App /></Router>, document.getElementById('app'));