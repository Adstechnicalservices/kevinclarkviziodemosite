
import React from 'react';

export class Disclaimer extends React.Component<{}, {}> {
    render() {
        return <disclaimer-message>
            <div>
            This website is for demonstration purposes and is not affiliated with Vizio Electronics.  Their official site may be found at: <a href="https://www.vizio.com/" target="_blank">https://www.vizio.com/</a>.

            </div>
        </disclaimer-message>
    }
}