import React from 'react';
import { Link } from 'react-router-dom';
import { Logo } from '../header/components/logo/logo';
export class Footer extends React.Component<{}, {}> {
    render() {
        return <footer>
            <div className="columns">
                <div className="column col-sm-12">
                    <Logo />
                </div>
                <div className="column col-xs-6 get-to-know-us-links">
                    <h6>Get To Know Us</h6>
                    <Link to="" >About Vizio</Link><br />
                    <Link to="" >Why Vizio</Link><br />
                    <Link to="" >Careers</Link><br />
                    <Link to="" >Vizio Blog</Link><br />
                    <Link to="" >Vizio And The Environment</Link><br />
                    <Link to="" >Accessibility</Link>
                </div>
                <div className="column col-xs-6">
                    <h6>Our Products</h6>
                    <Link to="" >TVs</Link><br />
                    <Link to="" >Sound Bars</Link><br />
                    <Link to="" >Speakers</Link><br />
                    <Link to="" >SmartCast</Link><br />
                    <Link to="" >Shop</Link>
                </div>
                <div className="column col-xs-6">
                    <h6>Get Help</h6>
                    <Link to="" >Support Center</Link><br />
                    <Link to="" >Contact Us</Link><br />
                    <Link to="" >Product Registration</Link><br />
                    <Link to="" >myVIZIO</Link><br />
                    <Link to="" >Safety Notice</Link>  <br />        
                    <Link to="" >Check Order Status</Link>            
                </div>
                <div className="column col-xs-6">
                    <h6>Community</h6>
                    <Link to="" >Facebook</Link><br />
                    <Link to="" >Twitter</Link><br />
                    <Link to="" >YouTube</Link><br />
                    <Link to="" >Instagram</Link>
                </div>
                <div className="column">
                    <h6>For Business</h6>
                    <Link to="" >Content Partners</Link><br />
                    <Link to="" >Affiliate Program</Link><br />
                    <Link to="" >Reseller</Link><br />
                    <Link to="" >Commercial</Link><br />
                    <Link to="" >Inscape</Link>
                </div>
            </div>
        </footer>

    }
}