import React from 'react';
import { Link } from 'react-router-dom';

export class AppSearch extends React.Component<{}, {}> {
    render() {
        return <app-search>
            <Link to="/cart" href="/cart">
                <svg className="icon">
                    <use xlinkHref="#magnifying-glass"></use>
                </svg>
            </Link>
        </app-search>
    }
}