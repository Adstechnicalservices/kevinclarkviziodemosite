import React from 'react';
import { Link } from 'react-router-dom';

export class CartLink extends React.Component<{}, {}> {
    render() {
        return <cart-link>
            <Link to="/cart" href="/cart">
                <svg className="icon">
                    <use xlinkHref="#shopping-cart-1"></use>
                </svg>
            </Link>
        </cart-link>
    }
}