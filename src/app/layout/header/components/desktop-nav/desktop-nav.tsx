import React from 'react';
import {Link} from 'react-router-dom';

export class DesktopNav extends React.Component<{}, {}> {
    render() {
        return <desktop-nav>
            <nav>
            <ul>
                <li>
                    <h6><Link to="/" href="/">Shop</Link></h6>
                </li>
                <li>
                   <h6> <Link to="/" href="/">TVs</Link></h6>
                </li>
                <li>
                  <h6> <Link to="/" href="/">Sound Bars</Link></h6> 
                </li>
                <li>
                  <h6> <Link to="/" href="/">Speakers</Link></h6> 
                </li>
                <li>
                   <h6> <Link to="/" href="/">SmartCast</Link></h6>
                </li>
                <li>
                    <h6><Link to="/contact" href="/contact">Support</Link></h6>
                </li>
            </ul>
        </nav>
        </desktop-nav>
    }
}