import React from 'react';

export class MobileNav extends React.Component<{}, {}> {
    private hamburger = React.createRef<HTMLButtonElement>();
    toggleHamburgerMenu() {
        if (this.hamburger.current) {
            this.hamburger.current.classList.toggle('is-active');
        }
    }
    render() {
        return <button className="hamburger hamburger--collapse" ref={this.hamburger} onClick={this.toggleHamburgerMenu.bind(this)} type="button">
                <span className="hamburger-box">
                    <span className="hamburger-inner"></span>
                </span>
            </button>
    }
}