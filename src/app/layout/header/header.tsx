import React from 'react';
import { DesktopNav } from './components/desktop-nav/desktop-nav';
import { Logo } from './components/logo/logo';
import { MobileNav } from './components/mobile-nav/mobile-nav';
import { CartLink } from './components/cart-link/cart-link';
import { AppSearch } from './components/app-search/app-search';


export class Header extends React.Component<{}, {}> {

    render() {
        return <header>

            <ul>

                <li className="logo">
                    <Logo />
                </li>

                <li className="main-nav-desktop">
                    <DesktopNav />
                </li>

                <li className="cart">
                    <CartLink />
                </li>

                <li className="search">
                    <AppSearch />
                </li>

                <li className="main-nav-mobile">
                    <MobileNav />
                </li>

            </ul>
        </header>
    }
}