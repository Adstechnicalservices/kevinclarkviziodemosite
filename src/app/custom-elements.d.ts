declare namespace JSX {
    interface IntrinsicElements {
        'app-routing': any;
        'app-logo': any;
        'disclaimer-message': any;
        'desktop-nav': any;
        'mobile-nav': any;
        'cart-link': any;
        'app-search': any;
        'app-page': any;
        'home-page': any;
        'contact-page': any;
    }
}