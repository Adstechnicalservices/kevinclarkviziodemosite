import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { HomePage } from './pages/home/home-page';
import { Page } from './pages/page';
import { ContactPage } from './pages/contact/contact-page';

export class App extends React.Component<{}, {}> {
    render() {
        return <app-routing>
            <Route exact path="/" component={HomePage}></Route>
            <Route path="/cart" component={HomePage}></Route>
            <Route path="/checkout" component={HomePage}></Route>
            <Route  path="/product-list" component={HomePage}></Route>
            <Route path="/product-detail" component={HomePage}></Route>
            <Route path="/contact" component={ContactPage}></Route>
        </app-routing>
    }
}