import React from 'react';
import { Header } from '../layout/header/header';
import { Footer } from '../layout/footer/footer';
import { Disclaimer } from '../layout/disclaimer/disclaimer';
interface Props {
    pageContent: JSX.Element
}
export class Page extends React.Component<Props, {}> {
    render() {
        return <app-page>
            <Disclaimer />
            <div className="container">
            <Header />
            <div className="page">
                {this.props.pageContent}
            </div>
            <Footer />
            </div>
        </app-page>
    }
}