import React from 'react';
import { Header } from '../../layout/header/header';
import { Footer } from '../../layout/footer/footer';
import { Page } from '../page';
import Slider from 'react-slick';
import { fadeIn } from '../../helpers';

export class HomePage extends React.Component<{}, {}> {

    componentDidMount() {
        let scrollY = 0;
        const feature1 = document.getElementsByClassName('p-series')[0] as HTMLDivElement;
        feature1.style.opacity= '0.0';
       

        const feature2 = document.getElementsByClassName('smart-assistants')[0] as HTMLDivElement;
        feature2.style.opacity = '0.0';

        const feature3 = document.getElementsByClassName('smart-tv')[0] as HTMLDivElement;
        feature3.style.opacity = '0.0';

        document.addEventListener('scroll', (event) => {
            scrollY = window.scrollY;
            if (scrollY >= 0 && scrollY <= 600) {
                console.log('Firist one fades');
                feature1.classList.add('animated', 'fadeIn', 'slower');
            } else if (scrollY > 600 && scrollY <= 1330) {
                console.log('Animate two');
                feature2.style.opacity = '1.0';
                feature2.classList.add('animated', 'fadeIn', 'slower');
            } else if (scrollY > 1330) {
                console.log('Animate 3');
                feature3.style.opacity = '1.0';
                feature3.classList.add('animated', 'fadeIn', 'slower');
            }
        });
        window.scrollTo(0, 1);
    }

    render() {

        return <Page pageContent={
            <home-page>
                
                <div className="columns p-series">
                    <div className="column col-sm-12">
                        <img src="./assets/img/slide-pq-series-desktop.png" />
                        <div className="product-highlight">
                            <h4>Introducing Vizio's Best Picture Ever</h4>
                            <h1>P-Series Quantam.</h1>
                            <h5>Over 1 billion colors.  Brightest picture.  Deepest blacks.</h5>
                            <h6>Size 65" | $2099</h6>
                            <span><button className="btn btn-link">Learn More</button></span> <span><button className="btn btn-primary">Shop Now</button></span>
                        </div>
                    </div>
                
                
                </div>
                
                <div className="columns smart-assistants">
               
                    <div className="column col-sm-12">
                        <img className="smart-assistants-tv-img" src="./assets/img/smart-assistants-tv.png" />
                    </div>
                    
                    <div className="column col-sm-12">
                        <div className="smart-assistants-description">
                            <h1> We bring it all together.  </h1>
                            <h6>Smarter experience with voice capabilities and Chromecast built in.</h6>
                            <div className="integrations">
                                <span>
                                    <img src="./assets/img/alexa.png" />
                                </span>
                                <span>
                                    <img src="./assets/img/google-assistant.png" />
                                </span>
                                <span>
                                    <img src="./assets/img/chromecast.jpg" />
                                </span>
                            </div>
                            <span><button className="btn btn-primary">Shop TV Series</button></span> <span><button className="btn btn-primary">Shop Sound Bars</button></span>
                        </div>
                    </div>
                    
                </div>
                <div className="columns smart-tv">
                    <div className="column col-sm-12">
                        <div className="smart-tv-description">
                            <h1> 2018 <br /> Smart TVs.  </h1>
                            <h6>Smarter in every series.</h6>
                            <div className="smart-tv-icon">
                                <svg>
                                    <use xlinkHref="#smart-tv"></use>
                                </svg>
                            </div>
                            <span><button className="btn btn-link">Learn More</button></span> <span><button className="btn btn-primary">Shop Now</button></span>
                        </div>
                    </div>
                    <div className="column col-sm-12">
                        <img src="./assets/img/smart-tv.jpg" />
                    </div>
                </div>
                
            </home-page>
        } />
    }
}