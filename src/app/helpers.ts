

export const fadeIn = (div: HTMLDivElement) => {
    div.style.opacity = '0.4'; 
    let opacityNum = Number(div.style.opacity);
    const timer = setInterval(() => {
        if (opacityNum >= 1.0) {
            clearInterval(timer);
        }
        opacityNum += 0.1;
        div.style.opacity = opacityNum.toString();
    }, 200);
}